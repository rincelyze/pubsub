package pubsub

type Channel struct {
	Name string
	Subs []Client
}

var Channels []Channel

func getChannelByName(name string) Channel {
	for _, v := range Channels {
		if (v.Name == name) {
			return v
		}
	}
	return Channel{Name:name}
}

func (chann Channel)Publish(message string) {
	for _, v := range chann.Subs {
		v.Write(chann.Name, message)
	}
}