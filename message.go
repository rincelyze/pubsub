package pubsub

import "time"

type Message struct {
	Channel string `json:"channel"`
	DateSent	time.Time `json:"date_sent"`
	Message string `json:"message"`
}

