package pubsub

import (
	"time"
	"math/rand"
	"encoding/json"
	"os"
)

type Client interface {
	Write(channel, message string) bool
	Connect()
	Subscribe(Channel)
	Unsubscribe(Channel)
}

type CoreClient struct {
	id string
}

// Local client is also suppose to listen
type LocalClient struct {
	CoreClient
	channel Channel
}


func (client LocalClient) Write(channel, message string) bool {

	return true
}

func (client LocalClient) Connect() {
	client.id = os.Getenv("pubsub_id")
	notifyOfNew(client)
	Clients = append(Clients, client)
}

func (client LocalClient) Subscribe(channel Channel) {

}


func (client LocalClient) Unsubscribe(channel Channel) {

}


func notifyOfNew(client Client) {
	json,_ := json.Marshal(client)
	for _, v := range Clients {
		v.Write("new-clients",string(json))
	}
}

func generateClientId() string {
	rand.Seed(time.Now().UTC().UnixNano())
	const chars = "abcdefghijklmnopqrstuvwxyz0123456789"
	result := make([]byte, 12)
	for i := 0; i < 12; i++ {
		result[i] = chars[rand.Intn(len(chars))]
	}
	return string(result)
}