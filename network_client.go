package pubsub

import (
	"net"
	"net/http"
	"fmt"
	"time"
	"encoding/json"
)

// Client connected through websocket (RFC 6455)
type SocketClient struct {
	CoreClient
	ip net.IPAddr
	w  http.ResponseWriter
}

// Regular REST client, we write to it using HTTP POST REQUEST
type HttpClient struct {
	CoreClient
	ip   net.IPAddr
	urlc string
}

var Clients []Client

func (client SocketClient) Connect() {
	for _, v := range Clients {
		Clients = append(Clients, v)
	}
}

func (client HttpClient) Connect() {
	for _, v := range Clients {
		Clients = append(Clients, v)
	}
}

func (client SocketClient) Write(channel, message string) bool {
	json,_ := json.Marshal(Message{Message:message, Channel:channel, DateSent:time.Now()})
	fmt.Println(json)
	return true
}

func (client HttpClient) Write(channel, message string) bool {

	return true
}

func (client SocketClient) Subscribe(channel Channel) {

}

func (client HttpClient) Subscribe(channel Channel) {

}

func (client SocketClient) Unsubscribe(channel Channel) {

}

func (client HttpClient) Unsubscribe(channel Channel) {

}

